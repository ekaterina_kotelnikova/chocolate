package ru.kev.chocolate;

import java.util.Scanner;

/**
 * Класс определяет максимальное количество шоколадок, которые можно купить и обменять их обёртки на  другие шоколадки.
 * @author Kotelnikova E.V. group 15it20
 */
public class Main {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите количество денег: ");
        int money = input();
        System.out.println("Введите цену за одну шоколадку: ");
        int price = input();
        System.out.println("Введите количество оберток: ");
        int wrap = input();
        if (wrap == 1) {
            System.out.println("Вы получите бесконечный запас шоколадок :P");
            System.exit(1);
        }
        if (price > money) {
            System.out.println("К сожалению, ваших денег не достаточно для покупки шоколадки.");
            System.exit(1);
        }

        System.out.println("Максимальное количество шоколадок, на которые вы можете рассчитывать - " +
                maxCountOfChocolate(money, price, wrap));

    }

    /**
     * Метод позволяет определить максимальное количество шоколадок, на которые может рассчитывать пользователь, с учетом
     * имеющихся денег, цены одной шоколадки и количества оберток, требующихся для обмена на шоколадку.
     *
     * @param money имеющиеся деньги
     * @param price цена одной шоколадки
     * @param wrap  количество оберток для обмена
     * @return максимальное количество шоколадок
     */
    public static int maxCountOfChocolate(int money, int price, int wrap) {
        int choco = money / price;
        int wraps = choco;
        while (wraps >= wrap) {
            choco = choco + wraps / wrap;
            wraps = wraps / wrap + wraps % wrap;
        }
        return choco;
    }

    /**
     * Метод для ввода числа(от 1) с клавиатуры.
     *
     * @return число, введённое с клавиатуры
     */
    public static int input() {
        int number;
        do {
            number = scanner.nextInt();
            if (number <= 0) {
                System.out.println("Введите число от 1. Повторите ввод:");
            }
        } while (number <= 0);
        return number;
    }
}
